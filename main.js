//load the lib 
var path = require("path");
var express = require("express");

//create instance of app
var app = express();


var allComments = [
   {id: 1, img: "images.jpg", cmt: ["comment a","time 1","user A"]},
   {id: 2, img: "Wedding2.png", cmt: []},
   {id: 3, img: "Wedding4.jpg", cmt: []}
]; // hb


var placeComment = function(commentDetails){ // hb
   console.log(commentDetails);
}

// routes by hb
app.get("/placeComment", function(req, res) { // cust client placing order
   
   allComments.push(req.query);
   
   console.log("All comments:\n %s", JSON.stringify(allComments));

   res.status(201).end();
});  

app.get("/showComment", function(req, res) {
   console.log("... showing comments...");
   res.status(200);
   res.type("application/json");
   res.json(allComments);
   console.log("allComments >> %s", allComments);
})
// end of hb routes
// start matt
var allGuest = [];


app.get("/register", function (req, resp) {


	var createList = function (ord) {
		return ({
			email: ord.email,
			username: ord.username,
			

		});
	}
	allGuest.push(createList(req.query));
	console.log("All Guest:\n %s", JSON.stringify(allGuest));

	resp.status(201).end();

});

app.get("/guestList", function (req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(allGuest);

});

//end matt





//load the static resources from the following directory
app.use (express.static(__dirname+ "/public"));
app.use("/images",express.static(path.join(__dirname + "/images")));
app.use("/libs",express.static(__dirname+"/bower_components"));

app.get("/register",function(req,resp){
    //console.log("MAIN.JS :- email: %s , username: %s, password: %s",req.query.email,req.query.username,req.query.password); 
    var status = (req.query.password == req.query.username)?202:400; 

    resp.status(status);
    if(202 == status){
        //Access the database 
        resp.type("application/json");
    }
    resp.end();
});


//wedding file
app.get("/wedding", function (req, resp) {
	resp.type("text/html");
	resp.status(200);
	res.redirect('/wedding.html');
	//resp.send( "/wedding.html" );
});


// for error page 
app.use(function (req, resp) {
	resp.status(404);
	resp.send("error file nothing to serve");
});

//define the port that our app is going to listen to 
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

//start the server 
app.listen(app.get("port"), function () {
	console.log("application started at %s on %d", new Date(), app.get("port"));
});