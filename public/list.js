(function () {
	var ListApp = angular.module("ListApp", []);

	var ListCtrl = function ($http) {
		var listCtrl = this;

		

		listCtrl.basket = [];


		listCtrl.listGuest = function () {


			$http.get("/guestList")
			.then(function(result){
				listCtrl.basket = result.data;
			}) ;
		};
	};


	ListApp.controller("ListCtrl", ["$http", ListCtrl]);


})();