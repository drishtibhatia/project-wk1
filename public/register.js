
//controller for index.html login page
(function () {

    var LoginApp = angular.module("LoginApp", []);

    var LoginCtrl = function ($http) {
        var loginCtrl = this;

        loginCtrl.email = "";
        loginCtrl.username = "";
        loginCtrl.attend = "";

        loginCtrl.status = "";
        loginCtrl.performLogin = function () {
            console.info("email: %s , username: %s, ", loginCtrl.email, loginCtrl.username, loginCtrl.attend);

            var prom = $http
                .get("/register", {
                    params: {
                        email: loginCtrl.email,
                        username: loginCtrl.username,
                        attend: loginCtrl.attend
                    }
                })
                .then(function (result) {
                    console.log(JSON.stringify(result.data));
                    loginCtrl.status = "Login sucessfull";

                })
                .catch(function (status) {
                    loginCtrl.status = "Username and Password incorrect";
                });
        }
    }

    LoginApp.controller("LoginCtrl", ["$http", LoginCtrl]);

})();